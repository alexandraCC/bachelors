import { ObjectWithStringIndexes } from '../model/types';

export default async (
  endpoint: string,
  body: ObjectWithStringIndexes|null,
  method: string = 'POST',
  authorizationToken: string | null = null,
) => {
  const headers = new Headers();
  if (authorizationToken) {
    headers.append('Authorization', `Bearer ${authorizationToken}`);
  }

  const response = await fetch(`${import.meta.env.VITE_APP_API_URL}/${endpoint}`, {
    method,
    headers,
    ...(body && { body: JSON.stringify(body) }),
  });

  return response.json();
};
