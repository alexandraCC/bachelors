import { createApp } from 'vue';
import App from './App.vue';
import './scss/index.scss';
import router from './router';
import '../node_modules/@fortawesome/fontawesome-free/js/solid.js';
import '../node_modules/@fortawesome/fontawesome-free/js/all.js';

createApp(App).use(router).mount('#app');
