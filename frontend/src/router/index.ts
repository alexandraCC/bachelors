import { createRouter, createWebHistory } from 'vue-router';
import fetch from '../util/fetch';

import Orders from '../views/Orders.vue';
import Products from '../views/Products.vue';
import Dashboard from '../views/Dashboard.vue';
import Login from '../components/auth/Login.vue';
import NewPassword from '../components/auth/NewPassword.vue';
import ResetPassword from '../components/auth/ResetPassword.vue';
import Clients from '../views/Clients.vue';
import Client from '../components/client/Client.vue';
import Product from '../components/product/Product.vue';
import Signup from '../components/auth/Signup.vue';
import Order from '../components/order/Order.vue';

const routes = [
  {
    path: '/orders',
    name: 'Orders',
    component: Orders,
  },
  {
    path: '/order/:id',
    name: 'Order',
    component: Order,
  },
  {
    path: '/products',
    name: 'Products',
    component: Products,
  },
  {
    path: '/product/:id',
    name: 'Product',
    component: Product,
  },
  {
    path: '/clients',
    name: 'Clients',
    component: Clients,
  },
  {
    path: '/client/:id',
    name: 'Client',
    component: Client,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/sign-up',
    name: 'Signup',
    component: Signup,
  },
  {
    path: '/new-password',
    name: 'NewPassword',
    component: NewPassword,
  },
  {
    path: '/reset-password',
    name: 'ResetPassword',
    component: ResetPassword,
  },
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

const isUserAdmin = async () => {
  if (!localStorage.getItem('userId') || !localStorage.getItem('accessToken')) {
    return false;
  }

  const content = await fetch(
    `users/${localStorage.getItem('userId')}`,
    null,
    'GET',
    localStorage.getItem('accessToken'),
  );

  if (content.error || !content.user) {
    return false;
  }

  return content.user.isAdmin;
};

router.beforeEach(async (to) => {
  const authenticationRoutes = ['Login', 'NewPassword', 'ResetPassword', 'Signup'];
  const nonAdminRoutes = ['Orders', 'Order', 'Login', 'NewPassword', 'ResetPassword', 'Signup'];

  if (authenticationRoutes.indexOf(to.name as string) === -1
      && !localStorage.getItem('accessToken')
  ) {
    return '/login';
  }

  if (authenticationRoutes.indexOf(to.name as string) !== -1
      && localStorage.getItem('accessToken')
      && localStorage.getItem('userId')
  ) {
    return '/';
  }

  if (nonAdminRoutes.indexOf(to.name as string) === -1 && !(await isUserAdmin())) {
    return '/orders';
  }

  return true;
});

export default router;
