export type ObjectWithStringIndexes = {
    [key: string]: any,
}

export type User = {
    id: number,
    name: string,
    email: string,
    isAdmin: number,
    clientId: number,
}

export type Client = {
    id: number,
    name: string,
    registrationNumber: string,
    email: string,
    addressLine1: string,
    addressLine2: string|null,
    zipCode: string,
    city: string,
    country: string,
    timeAdded: number,
    timeLastUpdated: number,
    accountRequestSent: boolean,
}

export type Product = {
    id: string,
    productCode: string,
    name: string,
    size: string,
    sellingPrice: number,
    productionPrice: number,
    timeAdded: number,
    timeLastUpdated: number,
    clientId?: string|null,
    clientName?: string|null,
}

export type OrderItem = {
    id?: string,
    productId: string,
    productCode?: string,
    productName?: string,
    productSize?: string,
    quantity: number|string,
    unitSellingPrice: number|string,
    unitProductionPrice: number|string,
    totalPrice?: number,
    orderId?: string,
    timeAdded?: number,
    timeLastUpdated?: number,
};

export type Order = {
    id: string,
    clientName?: string,
    clientId?: string,
    status: string,
    totalAmount: string,
    totalProductionAmount: string,
    timeAdded: number,
    timeLastUpdated: number
    preferredDeliveryDate: number,
}

// eslint-disable-next-line no-shadow
export enum OrderState {
    AwaitingApproval = 'Awaiting Approval',
    AwaitingProcessing ='Awaiting Processing',
    InProgress = 'In Progress',
    BeingDelivered = 'Being Delivered',
    Done = 'Done',
    Cancelled = 'Cancelled',
}
