module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:vue/essential',
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
  },
  plugins: [
    'vue',
    '@typescript-eslint',
  ],
  settings: {
    'import/resolver': {
      typescript: {}, // this loads tsconfig.json
    },
  },
  rules: {
    'vue/no-v-model-argument': 'off',
    'vue/no-unused-components': 'warn',
    'max-len': 'off',
    'no-unused-vars': 'warn',
    'import/extensions': 'off',
    'no-alert': 'off',
    'no-param-reassign': 'off',
  },
};
