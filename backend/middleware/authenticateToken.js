const jwt = require('jsonwebtoken');

const validateAppToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(' ')[1];

  jwt.verify(token, process.env.ACCESS_SECRET_TOKEN, (err, payload) => {
    if (err) {
      return res.status(403).send({ error: true, errorMessage: 'Could not validate request!' });
    }

    req.user = payload;
    return next();
  });
};

const validateEmailToken = (req, res, next) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(' ')[1];

  jwt.verify(token, process.env.ACCESS_EMAIL_SECRET_TOKEN, (err, payload) => {
    if (err) {
      return res.status(403).send({ error: true, errorMessage: 'Could not validate request!' });
    }

    req.payload = payload;
    return next();
  });
};

module.exports = {
  validateAppToken,
  validateEmailToken,
};
