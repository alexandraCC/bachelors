require('dotenv').config();
const express = require('express');
const rateLimiter = require('express-rate-limit');

const authRoutes = require('./routes/auth');
const clientsRoutes = require('./routes/clients');
const productsRoutes = require('./routes/products');
const usersRoutes = require('./routes/users');
const ordersRoutes = require('./routes/orders');
const orderItemsRoutes = require('./routes/orderItems');
const dashboardRoutes = require('./routes/dashboard');

const app = express();

// Body parser
app.use(express.json({ type: ['application/json', 'text/plain'] }));

// CORS headers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS, PATCH');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, authorization, Content-Type, Accept');
  next();
});

// Routes
app.use(authRoutes);
app.use(clientsRoutes);
app.use(productsRoutes);
app.use(usersRoutes);
app.use(ordersRoutes);
app.use(orderItemsRoutes);
app.use(dashboardRoutes);

// Rate limiting for auth routes
app.use('/auth', rateLimiter({
  windowMs: 5 * 60 * 1000, // 5 minutes
  max: 5, // limit each IP to 5 requests per 5 minutes
}));

const port = process.env.PORT ?? 8000;

app.listen(port, (error) => {
  if (error) {
    console.log(`Error occurred: ${error}`);
  }

  console.log(`App listening on port ${port}!`);
});
