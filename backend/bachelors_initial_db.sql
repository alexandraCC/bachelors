# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.32)
# Database: bachelors
# Generation Time: 2021-06-06 17:58:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `registrationNumber` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `addressLine1` varchar(255) NOT NULL,
  `addressLine2` varchar(255) DEFAULT NULL,
  `zipCode` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `timeAdded` bigint(255) NOT NULL,
  `timeLastUpdated` bigint(255) NOT NULL,
  `accountRequestSent` tinyint(1) NOT NULL DEFAULT '0',
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `registrationNumber` (`registrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table orderLines
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orderLines`;

CREATE TABLE `orderLines` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quantity` decimal(10,0) NOT NULL,
  `unitSellingPrice` decimal(10,2) NOT NULL,
  `unitProductionPrice` decimal(10,2) NOT NULL,
  `timeAdded` bigint(255) NOT NULL,
  `timeLastUpdated` bigint(255) NOT NULL,
  `orderId` bigint(20) NOT NULL,
  `productId` bigint(20) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL DEFAULT '',
  `totalAmount` decimal(10,2) DEFAULT NULL,
  `totalProductionAmount` decimal(10,2) DEFAULT NULL,
  `preferredDeliveryDate` bigint(255) DEFAULT NULL,
  `timeAdded` bigint(255) NOT NULL,
  `timeLastUpdated` bigint(255) NOT NULL,
  `clientId` bigint(20) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `productCode` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `size` varchar(255) NOT NULL DEFAULT '',
  `sellingPrice` decimal(10,2) NOT NULL,
  `productionPrice` decimal(10,2) NOT NULL,
  `timeAdded` bigint(255) NOT NULL,
  `timeLastUpdated` bigint(255) NOT NULL,
  `clientId` bigint(20) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `productCode` (`productCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `timeAdded` bigint(255) NOT NULL,
  `timeLastUpdated` bigint(255) NOT NULL,
  `clientId` bigint(20) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `hash`, `isAdmin`, `timeAdded`, `timeLastUpdated`, `clientId`)
VALUES
	(6,'Admin','corollapackagingorders@gmail.com','$2b$10$hT7C5Ucl0SWUlpHtBip8b.bpqK1Nf44E7YYc.n.Dw6BfpqfvRXiIy',1,1622284479420,1622284479420,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
