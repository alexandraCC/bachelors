const router = require('express').Router();
const databaseConnection = require('../database');
const { validateAppToken } = require('../middleware/authenticateToken');
const isUserAdmin = require('../middleware/isUserAdmin');

router.get('/clients', validateAppToken, isUserAdmin, (req, res) => {
  databaseConnection.execute(
    'SELECT * FROM clients ORDER BY timeAdded DESC',
    async (error, results) => {
      if (error) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      return res.status(200).send({ success: true, clients: results });
    },
  );
});

router.get('/clients/:id', validateAppToken, isUserAdmin, (req, res) => {
  const id = parseInt(req.params.id, 10);

  databaseConnection.execute(
    'SELECT * FROM clients WHERE id = ?',
    [id],
    async (error, results) => {
      if (error) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      return res.status(200).send({ success: true, client: results[0] });
    },
  );
});

router.post('/clients', validateAppToken, isUserAdmin, (req, res) => {
  const client = req.body;

  databaseConnection.execute(
    `INSERT INTO clients (id, name, registrationNumber, email, addressLine1, addressLine2, zipCode, city, country, timeAdded, timeLastUpdated) 
         VALUES (?,?,?,?,?,?,?,?,?,?,?)`,
    [
      null,
      client.name,
      client.registrationNumber,
      client.email,
      client.addressLine1,
      client.addressLine2 ?? null,
      client.zipCode,
      client.city,
      client.country,
      (new Date()).getTime(),
      (new Date()).getTime(),
    ],
    async (error, results) => {
      if (error) {
        return res.status(500).send({ error: true, errorMessage: 'Could not create a new client!' });
      }

      return res.status(200).send({ success: true, clientId: results.insertId });
    },
  );
});

module.exports = router;
