const router = require('express').Router();
const databaseConnection = require('../database');
const { validateAppToken } = require('../middleware/authenticateToken');

router.get('/orderItems', validateAppToken, (req, res) => {
  const { orderId } = req.query;

  databaseConnection.execute(
    `SELECT orderLines.quantity, orderLines.unitSellingPrice, orderLines.unitProductionPrice, orderLines.timeAdded, orderLines.orderId, orderLines.unitSellingPrice * orderLines.quantity as totalPrice, orderLines.productId, 
         products.id, products.productCode as productCode, products.name as productName, products.size as productSize
         FROM orderLines LEFT JOIN products ON orderLines.productId = products.id WHERE orderId=? ORDER by timeAdded DESC`,
    [orderId],
    (err, results) => {
      if (err) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      return res.status(200).send({ success: true, orderItems: results });
    },
  );
});

module.exports = router;
