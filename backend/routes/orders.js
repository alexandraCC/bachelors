const router = require('express').Router();
const databaseConnection = require('../database');
const { validateAppToken } = require('../middleware/authenticateToken');
const isUserAdmin = require('../middleware/isUserAdmin');
const orderStatus = require('../util/orderStatus');
const emailTransporter = require('../util/emailTransporter');

const getOrders = (clientId, callback) => {
  let sql = `SELECT orders.id, orders.status, orders.totalAmount, orders.totalProductionAmount, orders.preferredDeliveryDate, orders.timeAdded, orders.timeLastUpdated, clients.name as clientName 
    FROM orders LEFT JOIN clients ON orders.clientId = clients.id `;

  if (clientId !== null) {
    sql += 'WHERE orders.clientId=? ORDER BY orders.timeAdded DESC';
    return databaseConnection.execute(sql, [clientId], callback);
  }

  sql += 'ORDER BY orders.timeAdded DESC';
  return databaseConnection.execute(sql, callback);
};

const sendNewOrderEmail = async (orderId) => {
  await emailTransporter().sendMail({
    from: 'Corolla Packaging',
    to: 'corollapackagingorders@gmail.com',
    subject: 'New Order!',
    html: `
      <body>
        <h1>New Order!</h1>
        <p>A new order has been to the system. See order <a href="${process.env.VUE_APP_URL}/order/${orderId}">here</a>.</p>
      </body>
    `,
  });
};

router.get('/orders', validateAppToken, (req, res) => {
  const clientId = req.query.clientId ? parseInt(req.query.clientId, 10) : null;
  const userInfo = req.user;

  if (clientId && !userInfo.isAdmin && userInfo.clientId !== clientId) {
    return res.status(403).send({ error: true, errorMessage: 'Permission denied!' });
  }

  return getOrders(clientId, (err, results) => {
    if (err) {
      return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
    }

    return res.status(200).send({ success: true, orders: results });
  });
});

router.get('/orders/:id', validateAppToken, (req, res) => {
  const id = parseInt(req.params.id, 10);
  const userInfo = req.user;

  databaseConnection.execute(
    `SELECT orders.id, orders.status, orders.totalAmount, orders.totalProductionAmount, orders.preferredDeliveryDate, orders.timeAdded, orders.timeLastUpdated, orders.clientId, clients.name as clientName 
         FROM orders LEFT JOIN clients ON orders.clientId = clients.id WHERE orders.id = ?`,
    [id],
    async (error, results) => {
      if (error) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      const order = results[0];

      if (order.clientId !== userInfo.clientId && !userInfo.isAdmin) {
        return res.status(403).send({ error: true, errorMessage: 'Permission Denied!' });
      }

      return res.status(200).send({ success: true, product: results[0] });
    },
  );
});

router.post('/orders', validateAppToken, (req, res) => {
  const userInfo = req.user;
  const { clientId } = req.body;
  const { orderItems } = req.body;
  const { preferredDeliveryDate } = req.body;

  if (!userInfo.isAdmin && userInfo.clientId !== clientId) {
    return res.status(403).send({ error: true, errorMessage: 'Permission denied!' });
  }

  databaseConnection.beginTransaction((err) => {
    if (err) {
      return res.status(500).send({ error: true, errorMessage: 'Something went wrong. Try again!' });
    }

    databaseConnection.execute(`
      INSERT INTO orders (id, status, totalAmount, totalProductionAmount, preferredDeliveryDate, timeAdded, timeLastUpdated, clientId) 
      VALUES (?,?,?,?,?,?,?,?)`,
    [
      null,
      userInfo.isAdmin ? orderStatus.AwaitingProcessing : orderStatus.AwaitingApproval,
      null,
      null,
      preferredDeliveryDate ? (new Date(preferredDeliveryDate).getTime()) : null,
      (new Date()).getTime(),
      (new Date()).getTime(),
      clientId,
    ],
    (err, results) => {
      if (err) {
        return databaseConnection.rollback(() => res.status(500).send({ error: true, errorMessage: 'Something went wrong. Try again!' }));
      }

      const orderId = results.insertId;
      let orderTotalAmount = 0;
      let orderTotalProductionAmount = 0;

      orderItems.forEach((item) => {
        orderTotalAmount += Number(item.unitSellingPrice) * Number(item.quantity);
        orderTotalProductionAmount += Number(item.unitProductionPrice) * Number(item.quantity);

        databaseConnection.execute(`
          INSERT INTO orderLines (id, quantity, unitSellingPrice, unitProductionPrice, timeAdded, timeLastUpdated, orderId, productId) 
          VALUES (?,?,?,?,?,?,?,?)`,
        [
          null,
          Number(item.quantity),
          Number(item.unitSellingPrice),
          Number(item.unitProductionPrice),
          (new Date()).getTime(),
          (new Date()).getTime(),
          orderId,
          item.productId,
        ],
        (err) => {
          if (err) {
            return databaseConnection.rollback(() => res.status(500).send({ error: true, errorMessage: 'Something went wrong. Try again!' }));
          }
        });
      });

      databaseConnection.execute(
        'UPDATE orders SET totalAmount=?, totalProductionAmount=?, timeLastUpdated=? WHERE id=?',
        [
          orderTotalAmount,
          orderTotalProductionAmount,
          (new Date()).getTime(),
          orderId,
        ],
        (err) => {
          if (err) {
            return databaseConnection.rollback(() => res.status(500).send({ error: true, errorMessage: 'Something went wrong. Try again!' }));
          }

          databaseConnection.commit(async (err) => {
            if (err) {
              return databaseConnection.rollback(() => res.status(500).send({ error: true, errorMessage: 'Something went wrong. Try again!' }));
            }

            if (!userInfo.isAdmin) {
              try {
                await sendNewOrderEmail(orderId);
              } catch (e) {
                console.log(e);
              }
            }

            return res.status(200).send({ success: true, successMessage: 'Order added successfully!' });
          });
        },
      );
    });
  });
});

router.patch('/orders/:id', validateAppToken, isUserAdmin, (req, res) => {
  const { status } = req.body;
  const id = parseInt(req.params.id, 10);

  databaseConnection.query(
    'UPDATE orders SET status=?, timeLastUpdated=? WHERE id=?',
    [status, (new Date()).getTime(), id],
    async (error) => {
      if (error) {
        return res.status(500).send({ error: true, errorMessage: 'Could not update the order!' });
      }

      return res.status(200).send({ success: true });
    },
  );
});

module.exports = router;
