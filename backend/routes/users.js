const router = require('express').Router();
const databaseConnection = require('../database');
const { validateAppToken } = require('../middleware/authenticateToken');
const isUserAdmin = require('../middleware/isUserAdmin');

router.get('/users/:id', validateAppToken, (req, res) => {
  const id = parseInt(req.params.id, 10);

  databaseConnection.execute(
    'SELECT id, name, email, isAdmin, clientId FROM users WHERE id = ?',
    [id],
    (error, results) => {
      if (error) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      return res.status(200).send({ success: true, user: results[0] });
    },
  );
});

router.get('/users', validateAppToken, (req, res) => {
  const clientId = parseInt(req.query.clientId, 10);

  databaseConnection.execute(
    'SELECT id, name, email, clientId FROM users WHERE clientId = ?',
    [clientId],
    (error, results) => {
      if (error) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      return res.status(200).send({ success: true, users: results });
    },
  );
});

router.delete('/users', validateAppToken, isUserAdmin, (req, res) => {
  const clientId = parseInt(req.query.clientId, 10);

  databaseConnection.execute(
    'DELETE FROM users WHERE clientId = ?',
    [clientId],
    (error, results) => {
      if (error) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      return res.status(200).send({ success: true, users: results });
    },
  );
});

module.exports = router;
