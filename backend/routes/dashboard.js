const router = require('express').Router();
const databaseConnection = require('../database');
const { validateAppToken } = require('../middleware/authenticateToken');

router.get('/dashboard/orders-status', validateAppToken, (req, res) => {
  databaseConnection.execute(
    'SELECT status, COUNT(*) AS count FROM orders GROUP BY status',
    (err, results) => {
      if (err) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      const mappedResults = {};
      results.forEach((result) => {
        mappedResults[result.status] = result.count;
      });

      return res.status(200).send({ success: true, results: mappedResults });
    },
  );
});

module.exports = router;
