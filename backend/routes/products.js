const router = require('express').Router();
const databaseConnection = require('../database');
const { validateAppToken } = require('../middleware/authenticateToken');
const isUserAdmin = require('../middleware/isUserAdmin');

const getProducts = (clientId, callback) => {
  let sql = `SELECT products.id, products.name, products.productCode, products.size, products.sellingPrice, products.productionPrice, clients.name as clientName 
             FROM products LEFT JOIN clients ON products.clientId = clients.id `;

  if (clientId !== null) {
    sql += 'WHERE (products.clientId = ? OR products.clientId IS NULL) ORDER BY products.timeLastUpdated DESC';
    return databaseConnection.execute(sql, [clientId], callback);
  }

  sql += 'ORDER BY products.timeLastUpdated DESC';
  return databaseConnection.execute(sql, callback);
};

router.get('/products', validateAppToken, (req, res) => {
  const clientId = req.query.clientId ? parseInt(req.query.clientId, 10) : null;
  const userInfo = req.user;

  if (clientId && !userInfo.isAdmin && userInfo.clientId !== clientId) {
    return res.status(403).send({ error: true, errorMessage: 'Permission denied!' });
  }

  getProducts(clientId, (err, results) => {
    if (err) {
      return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
    }

    return res.status(200).send({ success: true, products: results });
  });
});

router.get('/products/:id', validateAppToken, (req, res) => {
  const id = parseInt(req.params.id, 10);

  databaseConnection.execute(
    'SELECT * FROM products WHERE id = ?',
    [id],
    (err, results) => {
      if (err) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      return res.status(200).send({ success: true, product: results[0] });
    },
  );
});

router.post('/products', validateAppToken, isUserAdmin, (req, res) => {
  const product = req.body;

  databaseConnection.execute(
    `INSERT INTO products (id, productCode, name, size, sellingPrice, productionPrice, timeAdded, timeLastUpdated, clientId) 
         VALUES (?,?,?,?,?,?,?,?,?)`,
    [
      null,
      product.productCode,
      product.name,
      product.size,
      product.sellingPrice,
      product.productionPrice,
      (new Date()).getTime(),
      (new Date()).getTime(),
      product.clientId ?? null,
    ],
    (err, results) => {
      if (err) {
        if (err.code === 'ER_DUP_ENTRY') {
          return res.status(500).send({ error: true, errorMessage: 'Duplicate entry for product!' });
        }
        return res.status(500).send({ error: true, errorMessage: 'Could not create a new product!' });
      }

      return res.status(200).send({ success: true, product: results.insertId });
    },
  );
});

router.patch('/products/:id', validateAppToken, isUserAdmin, (req, res) => {
  const { productionPrice, sellingPrice } = req.body;
  const id = parseInt(req.params.id, 10);

  databaseConnection.query(
    'UPDATE products SET productionPrice=?, sellingPrice=?, timeLastUpdated=? WHERE id=?',
    [productionPrice, sellingPrice, (new Date()).getTime(), id],
    (err) => {
      if (err) {
        return res.status(500).send({ error: true, errorMessage: 'Could not update the product!' });
      }

      return res.status(200).send({ success: true });
    },
  );
});

module.exports = router;
