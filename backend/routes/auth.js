const router = require('express').Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const databaseConnection = require('../database');
const emailTransporter = require('../util/emailTransporter');
const { validateEmailToken } = require('../middleware/authenticateToken');

const saltRounds = 10;
const hashPassword = async (password) => bcrypt.hash(password, saltRounds);
const authenticateUser = async (password, hash) => bcrypt.compare(password, hash);

const createUser = async (name, email, password, clientId, callback) => {
  const passwordHash = await hashPassword(password);

  return databaseConnection.execute(
    `INSERT INTO users (id, name, email, hash, isAdmin, timeAdded, timeLastUpdated, clientId)
     VALUES (?, ?, ?, ?, ?, ?, ?, ?)`,
    [
      null,
      name,
      email,
      passwordHash,
      0,
      (new Date()).getTime(),
      (new Date()).getTime(),
      clientId ?? null,
    ], callback,
  );
};

const sendEmailToResetPassword = async (emailTo, token) => {
  await emailTransporter().sendMail({
    from: 'Corolla Packaging Auth🔑 <auth@corollaPackaging.com>',
    to: `${emailTo}`,
    subject: 'Reset password',
    html: `
      <body>
        <h1>Reset password</h1>
        <p>
          You have submitted a request to reset your password. 
          Reset the password by clicking <a href="${process.env.VUE_APP_URL}/new-password?token=${token}">here</a>.
        </p>
      </body>`,
  });
};

const sendEmailToCreateClientAccount = async (emailTo, clientName, token) => {
  await emailTransporter().sendMail({
    from: 'Corolla Packaging Auth🔑 <auth@corollaPackaging.com>',
    to: `${emailTo}`,
    subject: 'Create Client Account',
    html: `
      <body>
        <h1>Client Account</h1>
        <p>You have received this email because Corolla Packaging SRL has marked you as one of their customers. To create an account click <a href="${process.env.VUE_APP_URL}/sign-up?token=${token}&email=${emailTo}&name=${clientName}">here</a>.</p>
      </body>`,
  });
};

router.post('/auth/sign-up', (req, res) => {
  const user = req.body;

  createUser(user.name, user.email, user.password, user.clientId, (err, results) => {
    if (err) {
      if (err.code === 'ER_DUP_ENTRY') {
        return res.status(500).send({ error: true, errorMessage: 'Duplicate entry for email. Try with a different email address.' });
      }

      return res.status(500).send({ error: true, errorMessage: 'Could not create a new user! Try again!' });
    }

    return res.status(200).send({ success: true, userId: results.insertId });
  });
});

router.post('/auth/login', async (req, res) => {
  const { email, password } = req.body;

  databaseConnection.execute(
    'SELECT id, hash, isAdmin, clientId FROM users WHERE email = ?',
    [email],
    async (err, results) => {
      if (err) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      if (!results.length) {
        return res.status(401).send({ error: true, errorMessage: 'Invalid credentials!' });
      }

      const { hash } = results[0];
      const isAuthenticated = await authenticateUser(password, hash);

      if (!isAuthenticated) {
        return res.status(401).send({ error: true, errorMessage: 'Invalid credentials!' });
      }

      const accessToken = jwt.sign(
        {
          id: results[0].id, email, isAdmin: results[0].isAdmin, clientId: results[0].clientId,
        },
        process.env.ACCESS_SECRET_TOKEN,
      );

      return res.status(200).send({ success: true, accessToken, userId: results[0].id });
    },
  );
});

router.post('/auth/reset-password', async (req, res) => {
  const { email } = req.body;

  databaseConnection.execute(
    'SELECT id FROM users WHERE email = ?',
    [email],
    async (err, results) => {
      if (err) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      if (!results.length) {
        return res.status(401).send({ error: true, errorMessage: 'Invalid credentials!' });
      }

      const token = jwt.sign({ userId: results[0].id }, process.env.ACCESS_EMAIL_SECRET_TOKEN);

      try {
        await sendEmailToResetPassword(email, token);
      } catch (e) {
        return res.status(500).send({ error: true, errorMessage: 'Could not send a password reset email!' });
      }

      return res.status(200).send({ success: true, successMessage: 'Please verify your email to reset the password!' });
    },
  );
});

router.post('/auth/new-password', validateEmailToken, async (req, res) => {
  const { userId } = req.payload;
  const { password } = req.body;
  const hash = await hashPassword(password);

  databaseConnection.execute(
    'UPDATE users SET hash = ? WHERE id = ?',
    [hash, userId],
    async (err) => {
      if (err) {
        return res.status(500).send({ error: true, errorMessage: 'Something went wrong!' });
      }

      return res.status(200).send({ success: true });
    },
  );
});

router.post('/auth/client-account-request', async (req, res) => {
  const id = parseInt(req.body.id, 10);

  databaseConnection.execute(
    'SELECT * FROM clients WHERE id = ?',
    [id],
    async (err, results) => {
      if (err) {
        return res.status(404).send({ error: true, errorMessage: 'Could not find client id.' });
      }

      const client = results[0];
      const token = jwt.sign(
        { clientId: client.id, clientEmail: client.email, clientName: client.name },
        process.env.ACCESS_EMAIL_SECRET_TOKEN,
      );

      try {
        await sendEmailToCreateClientAccount(client.email, client.name, token);
        databaseConnection.query(
          'UPDATE clients SET accountRequestSent=?, timeLastUpdated=? WHERE id=?',
          [1, (new Date()).getTime(), client.id],
        );
      } catch (e) {
        return res.status(500).send({ error: true, errorMessage: 'Could not send the email!' });
      }

      return res.status(200).send({ success: true, successMessage: 'Email sent successfully!' });
    },
  );
});

router.post('/auth/create-client-account', validateEmailToken, async (req, res) => {
  const user = req.body;
  const { payload } = req;

  createUser(user.name, user.email, user.password, payload.clientId, (err, results) => {
    if (err) {
      if (err.code === 'ER_DUP_ENTRY') {
        return res.status(500).send({ error: true, errorMessage: 'Duplicate entry for email. Try with a different email address.' });
      }

      return res.status(500).send({ error: true, errorMessage: 'Could not create a new user! Try again!' });
    }

    return res.status(200).send({ success: true, userId: results.insertId });
  });
});

module.exports = router;
