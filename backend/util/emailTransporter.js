require('dotenv').config();
const nodemailer = require('nodemailer');

module.exports = () => nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port: 587,
  secure: false,
  auth: {
    user: 'corollapackagingorders@gmail.com',
    pass: process.env.SMTP_PASSWORD,
  },
});
