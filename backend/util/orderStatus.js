module.exports = {
  AwaitingApproval: 'Awaiting Approval',
  AwaitingProcessing: 'Awaiting Processing',
  InProgress: 'In Progress',
  BeingDelivered: 'Being Delivered',
  Done: 'Done',
  Cancelled: 'Cancelled',
};
